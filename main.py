# Component Class
class Employee:
    def get_name(self):
        pass

    def get_salary(self):
        pass

# Leaf Class
class Developer(Employee):
    def __init__(self, name, salary):
        self.name = name
        self.salary = salary

    def get_name(self):
        return self.name

    def get_salary(self):
        return self.salary

# Composite Class
class Department(Employee):
    def __init__(self, name):
        self.name = name
        self.employees = []

    def add_employee(self, employee):
        self.employees.append(employee)

    def get_name(self):
        return self.name

    def get_salary(self):
        total_salary = sum(employee.get_salary() for employee in self.employees)
        return total_salary

# Client Code
developer1 = Developer('Jon Doe', 5000)
developer2 = Developer('Jane Smith', 7000)
designDepartment = Department('Design Department')
designDepartment.add_employee(developer1)
designDepartment.add_employee(developer2)
manager = Developer('Manager', 8000)
engineeringDepartment = Department('Engineering Department')
engineeringDepartment.add_employee(designDepartment)
engineeringDepartment.add_employee(manager)
print("Total salary of Engineering Department:", engineeringDepartment.get_salary())
